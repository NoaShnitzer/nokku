import React from 'react'
import './dashboard.css'
import {Table, TableBody, TableCell , TableContainer, TableHead , TableRow} from '@material-ui/core'
import { BarChart, People, ShowChart } from "@material-ui/icons";


const columns = [
    { field: 'brandName', headerName: 'Brand Name', width: 130 },
    { field: 'startDate', headerName: 'Start Date', type: Date, width: 130 },
    { field: 'targetAmountOfUsers', headerName: 'Target Amount Of Users Acquisitions', width: 130 },
    { field: 'status', headerName: 'Status', width: 130 },
  ];
  
  const rows = [
    { 'brandName': 'Vichy', 'startDate': '12/05/21' ,'targetAmountOfUsers': '200k' , 'status': 'Draft' },
    { 'brandName': 'Lancome', 'startDate': '12/06/21' ,'targetAmountOfUsers': '220k', 'status': 'In Review' },
    { 'brandName': 'La Roche-Posay', 'startDate': '12/12/21' ,'targetAmountOfUsers': '150k' , 'status': 'Active' },

  ];

export default function dashboard() {
    

    return (
        <div>
            <div className="dashboard" >

                <div className="dashboardItem">
                    <div >
                        <span className="mainInfo" >3k <BarChart/> </span>
                        <span className="dashboardTitle"> Acquired Users </span>
                    </div>
                </div>

                <div className="dashboardItem">
                    <div>
                        <span className="mainInfo" >40% <ShowChart/></span>
                        <span className="dashboardTitle"> ROI </span>
                    </div>
                </div>

                <div className="dashboardItem">
                    <div>
                        <span className="mainInfo" >1.5k <People/></span>
                        <span className="dashboardTitle"> subscribers </span>
                    </div>
                </div>

                <div className="dashboardItem">
                    <div>
                        <span className="mainInfo" >76% <ShowChart/></span>
                        <span className="dashboardTitle"> communications open rate </span>
                    </div>

                </div>
        
            </div>

            <div className="table">
                <Table aria-label="simple table" rowsPerPageOptions={[5]}
                    checkboxSelection>
                    <TableHead >
                        <TableRow>
                        <TableCell >Brand Name</TableCell>
                        <TableCell align="right">Start Date</TableCell>
                        <TableCell align="right">Target Amount Of Users Acquisitions</TableCell>
                        <TableCell align="right">Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                        <TableRow
                            key={row.name}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                            <TableCell component="th" scope="row">
                            {row.brandName}
                            </TableCell>
                            <TableCell align="right">{row.startDate}</TableCell>
                            <TableCell align="right">{row.targetAmountOfUsers}</TableCell>
                            <TableCell align="right">{row.status}</TableCell>
                        </TableRow>
                            ))}
                    </TableBody>
                </Table>
           </div>      
    </div>
    )
}
